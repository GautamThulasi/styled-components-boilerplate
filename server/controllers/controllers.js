const MOCK = require('../mock/mock_data.json');
const CAR_MOCK = require('../mock/CAR_MOCK.json');

const saySomething = (req, res, next) => {
	res.status(200).json({
		body: 'Hello from the server!'
	});
};

const news = (req, res, next) => {
	const { page } = req.body;
	setTimeout(() => {
		res.status(200).json({
			article: MOCK[page]
		});
	}, 1400);
};

const productsListing = (req, res, next) => {
	const { page, pageSize } = req.body;
	const rangeCompleted = page * pageSize;
	const dataArr = CAR_MOCK.slice(rangeCompleted, rangeCompleted + pageSize);
	setTimeout(() => {
		res.status(200).json({
			products: dataArr,
			totalProductsList: CAR_MOCK.length / pageSize
		});
	}, 1400);
};

module.exports = { news, saySomething, productsListing };
