const express = require('express');
const router = express.Router();
const controllers = require('./../controllers/controllers');

router.get('/say-something', controllers.saySomething);
router.put('/news', controllers.news);
router.put('/productsListing', controllers.productsListing);

module.exports = router;
