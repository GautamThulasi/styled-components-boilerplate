import { ThemeProvider, GlobalStyles, theme } from './style';
import Home from './containers/Home';
import AppLayout from './containers/AppLayout';
import { GlobalContextProvider } from './context/GlobalContext';

function App() {
	return (
		<div className="App">
			<ThemeProvider theme={theme.main}>
				<GlobalStyles />
				<GlobalContextProvider>
					<AppLayout />
				</GlobalContextProvider>
			</ThemeProvider>
		</div>
	);
}

export default App;
