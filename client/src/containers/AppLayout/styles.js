import { styled, css } from '../../style';
import { Link } from 'react-router-dom';

export const AppBanner = styled.div`
	min-height: 40px;
	background: ${({ theme }) => theme.colors.feedback.banner};
	display: flex;
	justify-content: center;
	align-items: center;
`;

export const AppHeader = styled.header`
	min-height: 50px;
	background: ${({ theme }) => theme.colors.primary.base};
	color: ${({ theme }) => theme.colors.neutrals['1']};
	display: flex;
	justify-content: space-between;
	padding: 30px 33px 0 20px;
`;

export const NavItem = styled.div`
	padding: 3px 10px 10px 0;
	margin: 0 12px 0px 6px;
	cursor: pointer;
	&:hover {
		opacity: .8;
		border-radius: 4px;
		/* box-shadow: 2px 10px 21px 0px rgba(20, 57, 50, 0.35); */
	}
`;

export const HeaderContainer = styled.div`
	padding: 3px 10px 10px 0;
	margin: 0 12px 0px 6px;
	cursor: pointer;
	&:hover {
		opacity: .8;
		border-radius: 4px;
		/* box-shadow: 2px 10px 21px 0px rgba(20, 57, 50, 0.35); */
	}
	display: flex;
	justify-content: space-between;
`;
export const AppFooter = styled.div;

export const StyledCartButton = styled(Link)`
	color: #ffffff;
	border: 0;
	cursor: pointer;
	border-radius: 4px;
	background-color: ${({ theme }) => theme.colors.button.info};
	font-size: 20px;
	padding-bottom: 10px;
	text-decoration: auto;
	margin-top: 15px;
	padding:5px;
	&:hover {
		opacity: .8;
	}
`;
