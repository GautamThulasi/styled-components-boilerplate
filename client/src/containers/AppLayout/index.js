import React from 'react';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import { AppBanner, AppHeader, StyledCartButton } from './styles';
import Home from '../Home';
import Cart from '../Cart';
import Product from '../Product'
import Text from '../../components/Text';
import { useTranslation } from 'react-i18next';
import {GlobalContext} from '../../context/GlobalContext'
import {useContextFactory} from '../../context/useContextFactory'

export default function AppLayout() {
	const { t } = useTranslation();
	const useGlobalState = useContextFactory('GlobalContext',GlobalContext);
	const headerNavItems = [ 'React Context API Sample' ];
	return (
		<React.Fragment>
			<AppBanner>
				<Text type={'Heading-5'}>{t("banner")}</Text>
			</AppBanner>
			<BrowserRouter>
			<AppHeader>
		<div>
		{
		headerNavItems?.map((item, key)=>(
			<StyledCartButton to={'/'} key={`${Math.random()+key}`} style={{paddingTop:7}}>
				{t(`header.${item}`)}
			</StyledCartButton>))
		}
		</div>
		<div>
		<StyledCartButton to={'/cart'}>{t('cart')}</StyledCartButton>
		</div>

</AppHeader>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/cart" component={Cart} />
        <Route path="/product/:id" component={Product} />

      </Switch>
    </BrowserRouter>
		</React.Fragment>
	);
}
