import React from 'react';
import {useParams} from 'react-router-dom'
import Card from '../../components/Card'
import { GlobalContext } from '../../context/GlobalContext';
import { useContextFactory } from '../../context/useContextFactory';

export default function Product() {
  const {id} = useParams();
  const useGlobalState = useContextFactory('GlobalContext', GlobalContext);
  const { state, dispatch } = useGlobalState();
  const {products}= state

  console.log("ID!!!!!!!!!!", id, products)
  const product = products.find((item, index)=>item?.id==id)
  console.log("PRODUCT", product)
  return (
    <>
    <div>
    <Card 
          onClick={()=>{
                    }}
          addToCart={(item)=>{
            dispatch({type:"UPDATE_CART", payload:item})}}  item={product}/>
    </div>
    </>
  );
}
