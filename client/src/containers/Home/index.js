import React from 'react';
import TextField from '../../components/Text';
import { useTranslation } from 'react-i18next';
import { HomeContainer, HomeCol } from './styles';
import { useContextFactory } from '../../context/useContextFactory';
import { GlobalContext } from '../../context/GlobalContext';
import ProductListing from '../ProductListing';
import Cart from '../Cart';

export default function Home() {
	const { t, i18n } = useTranslation();
	const useGlobalState = useContextFactory('GlobalContext', GlobalContext);
	const { state, dispatch } = useGlobalState();
	const { page = 0 } = state;
	return (
		<React.Fragment>
			<HomeCol flex={1}>
				<ProductListing />
				<div style={{ marginTop: 100 }}>
					<select
						onChange={(e) => {
							i18n.changeLanguage(e.target.value);
						}}
					>
						<option value="en" name="en">
							ENGLISH
						</option>
						<option value="fr" name="fr">
							FRENCH
						</option>
						<option value="de" name="de">
							GERMAN
						</option>
					</select>
				</div>
			</HomeCol>
		</React.Fragment>
	);
}
