import { styled, css } from '../../style';

export const HomeContainer = styled.div`
	display: flex;
	min-height: 100vh;
	justify-content: space-around;
`;

export const HomeCol = styled.div`
	padding: 20px 20px 20px 20px;
	flex: ${({ theme, flex = 1 }) => flex};
`;

export const GetNewsButton = styled.button`
	color: #ffffff;
	background-color: ${({ theme }) => theme.colors.button.primary};
	border: 0;
	cursor: pointer;
	border-radius: 4px;
	margin: 10px 10px 10px 0;
	width: 100%;
	height: 40px;
	&:hover {
		opacity: .8;
	}
`;
