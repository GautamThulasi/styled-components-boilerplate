import React from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import { useTranslation } from 'react-i18next';
import { useHistory } from "react-router-dom";
import { useContextFactory } from '../../context/useContextFactory';
import Card from '../../components/Card'
import { StyledCardContainer } from './style';
import Text from '../../components/Text'
import Pagination from '../../components/Pagination'

export default function ProductListing() {
	const useGlobalState = useContextFactory('GlobalContext', GlobalContext);
  const { state, dispatch } = useGlobalState();
  const{products=[], totalProductsList, page} = state
  let history = useHistory();

  if(state.products.length>0)
  return (
    <>
    <Text type={"Heading-5"}> INVENTORY</Text>
    <StyledCardContainer>

      {
        products.map((product, index)=>{
          return <Card key={index} 
          onClick={()=>{
            console.log("Click", product)
            history.push(`/product/${product.id}`);
          }}
          addToCart={(item)=>{
            dispatch({type:"UPDATE_CART", payload:item})}}  item={product}/>})
      }

      </StyledCardContainer>
      <Pagination currentPage = {page} total={totalProductsList} selectPage={(page)=>{ dispatch({type:"UPDATE", payload:{page}})}}/>

    </>
  );

  return<div>Loading Cars</div>
}


