import { styled, css } from '../../style';

export const StyledCardContainer = styled.div`
	display: flex;
	flex-wrap: wrap;
`;
