import React from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import { useTranslation } from 'react-i18next';
import { useContextFactory } from '../../context/useContextFactory';
import CartItem from '../../components/CartItem'
import { CartContainer } from './style';
import Text from '../../components/Text'
export default function Cart() {
	const useGlobalState = useContextFactory('GlobalContext', GlobalContext);
  const { state, dispatch } = useGlobalState();
  const{cart=[]} = state;
console.log("CART!!!!", state)
  const removeFromCart=(item)=>{
dispatch({type:"REMOVE_FROM_CART", payload:item})
  }
  if (cart??[].length>0)
  return (
    <>
    <CartContainer>
    <Text type={"Heading-5"}> CART</Text>

        {
          cart.map((cartItem, index)=>{
            return <CartItem key={index} item={cartItem} removeFromCart={()=>removeFromCart(cartItem)}/>
          })
        }
        
    </CartContainer>
    </>
  );
  return (<CartContainer>NO ITEMS IN CART</CartContainer>)
}
