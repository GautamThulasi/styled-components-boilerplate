import React from 'react';
import {GlobalContext} from '../../context/GlobalContext'
import {useContextFactory} from '../../context/useContextFactory'
import Text from '../../components/Text';

export default function NewsResult() {
  const useGlobalState = useContextFactory('GlobalContext', GlobalContext);
	const { state } = useGlobalState();
  const { page = 0, article, loadingArticles, showNews } = state;
          if(loadingArticles) return <Text type={'Heading-3'}>{'Loading'}</Text> 
          if(showNews)
  return (
    <>
					<Text type={'Heading-5'}>ARTICLE ID: {article?.id}</Text>
					<Text type={'Heading-5'}>TITLE: {article?.title}</Text>
					<Text type={'Heading-5'}>DESCRIPTION {article?.description}</Text>
    </>)
    else return null;
  
}
