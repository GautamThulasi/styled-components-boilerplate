const theme = {
	main: {
		layout: {
			columnGap: 24,
			rowGap: 6
		},
		colors: {
			primary: {
				base: '#3071A9',
				dark: '#4E0D3A',
				mid: '#720D5D',
				light: '#ffffff'
			},
			secondary: {
				base: '#E30425'
			},
			feedback: {
				red: '#f44336',
				green: '#4caf50',
				yellow: '#ffc107',
				info: '#2251ff',
				banner: '#FFB50F'
			},
			backgroundCard: {
				base: '#f0f3f8'
			},
			button: {
				primary: '#007bff',
				secondary: '#6c757d',
				success: '#28a745',
				danger: '#dc3545',
				warning: '#ffc107',
				info: '#17a2b8'
			},
			neutrals: {
				'11': '#d0daef',
				'10': '#000',
				'9': '#333',
				'8': '#4d4d4d',
				'7': '#757575',
				'6': '#b3b3b3',
				'5': '#d0d0d0',
				'4': '#e6e6e6',
				'3': '#f0f0f0',
				'2': '#fafafa',
				'1': '#ffffff'
			}
		},
		text: {
			font: {
				fontFamily: 'Calibri'
			},
			colors: {
				normal: '#4d4d4d'
			},
			types: {
				'Heading-1': `
        font-size: 96px;
        font-weight: 300;
        line-height: 1.18;
        `,
				'Heading-2': `
        font-size: 60px;
        font-weight: 400;
        line-height: 1.12;
        `,
				'Heading-3': `
        font-size: 48px;
        font-weight: 600;
        line-height: 1.22;
        `,
				'Heading-4': `
        font-size: 34px;
        font-weight: 600;
        line-height: 1.125;
        `,
				'Heading-5': `
        font-size: 24px;
        font-weight: 600;
        line-height: 1.142;
        `,
				'Heading-6': `
        font-size: 20px;
        font-weight: 600;
        line-height: 1.166;
        `,
				'Subtitle-1': `
        font-size: 16px;
        font-weight: 500;
        line-height: 1.12;
        `,
				'Subtitle-2': `
        font-size: 14px;
        font-weight: 600;
        line-height: 1.6;
        `,
				'Body-Default': `
                font-size: 16px;
        font-weight: 600;
        line-height: 1.166;
        `,
				'Body-Small': `
                font-size: 14px;
                font-weight: 400;
                line-height: 1.166;
        `
			}
		}
	}
};

export default theme;
