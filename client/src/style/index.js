import baseStyled, { css as baseCss, createGlobalStyle, ThemeProvider } from 'styled-components';
import theme from './theme';
export const GlobalStyles = createGlobalStyle`
body{
    background: ${({ theme }) => theme.colors.primary.light};
    color: ${({ theme }) => theme.text.colors.normal};
    font-family:${({ theme }) => theme.text.font.fontFamily};
}
`;

export const styled = baseStyled;

export const css = baseCss;

export { ThemeProvider };

export { theme };
