export const REDUCER = (state, action) => {
	const { payload } = action;
	switch (action.type) {
		case 'UPDATE': {
			return {
				...state,
				...payload
			};
		}
		case 'UPDATE_CART': {
			let { cart = [], cartItemIndices = [] } = state;
			const indexOfItemInCart = cartItemIndices.indexOf(payload.id);

			if (indexOfItemInCart !== -1) {
				cart[indexOfItemInCart] = payload;
			} else {
				cart = [ ...cart, payload ];
				cartItemIndices = [ ...cartItemIndices, payload.id ];
			}
			return {
				...state,
				cartItemIndices,
				cart
			};
		}
		case 'REMOVE_FROM_CART':
			let { cart = [], cartItemIndices = [] } = state;
			const indexOfItemInCart = cartItemIndices.indexOf(payload.id);
			console.log('CART@@@@', cart, cartItemIndices, payload);
			if (indexOfItemInCart !== -1) {
				cart.splice(indexOfItemInCart, 1);
				cartItemIndices.splice(indexOfItemInCart, 1);
			}
			return {
				...state,
				cartItemIndices,
				cart
			};

		default:
			return state;
	}
};
