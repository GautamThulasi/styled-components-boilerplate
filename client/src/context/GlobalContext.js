import React, { useEffect, useMemo,  useReducer, createContext, useCallback } from 'react';
import {REDUCER} from './Reducer';
import * as ProductServices from '../services/products'
const GlobalContext = createContext();
const GlobalContextProvider = (props) => {
	const [ state, dispatch ] = useReducer(REDUCER, {
        products:[],
        currentProduct:null,
        loading:false,
        loadingProducts: false,
        page: 0,
        showNews:false,
        cart:[],
        cartItemsIndices:[]
    });
    const fetchProducts = useCallback(async() => {
        try{
            dispatch({type:'UPDATE', payload:{loadingProducts:true}});
            const response = await ProductServices.productsListing(state?.page);
            dispatch({type:'UPDATE', payload:{loadingProducts:false, products:response.data.products, totalProductsList: response.data.totalProductsList}});
        }
        catch(err)
        {
            dispatch({type:'UPDATE', payload:{loadingProducts:false}});
        }
        }, [state?.page]);



	useEffect(() => {
        state.page !== null && fetchProducts(state.page)
    }, [state.page, fetchProducts]);
    
	const value = useMemo(() => {
        return {state, dispatch}
    }, [ state ]);
    if(state?.loading)
    return <div>Loading Indicator</div>

    return <GlobalContext.Provider value={value}>
{props.children}
    </GlobalContext.Provider>
};



export { GlobalContextProvider, GlobalContext}
