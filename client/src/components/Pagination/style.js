import { css, styled } from '../../style';

export const StyledPaginationContainer = styled.div`
	margin: 10px;
	padding: 10px;
	font-stretch: normal;
	font-style: normal;
`;

export const StyledPaginationItem = styled.button`
	color: #ffffff;
	font-weight: 600;
	border: 0;
	cursor: pointer;
	border-radius: 4px;
	width: 25px;
	height: 25px;
	margin: 10px;
	background-color: ${({ theme, active }) => (active ? theme.colors.button.success : theme.colors.button.info)};
	font-size: 14px;
	border-radius: 4px;
	flex: 0 0 25%;
	&:hover {
		opacity: .8;
	}
`;
