import React from 'react';
import { StyledPaginationContainer, StyledPaginationItem } from './style';

const Pagination = ({ total, currentPage, selectPage }) => {
	return (
		<StyledPaginationContainer>
			{[ ...Array(total).keys() ].map((item, index) => (
				<StyledPaginationItem
					onClick={() => {
						selectPage(item);
					}}
					key={index}
					active={currentPage === item}
				>
					{item}
				</StyledPaginationItem>
			))}
		</StyledPaginationContainer>
	);
};

export default Pagination;
