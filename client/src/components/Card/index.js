import React from 'react';
import { StyledCard, AddToCart, QtyCounter, CardItemElement } from './style';
import Text from '../Text';
import { useTranslation } from 'react-i18next';

const Card = ({ item, addToCart, onClick }) => {
	const { car_color, car_make, car_model, car_price, car_vin, car_year, id } = item;
	const [ qty, setQty ] = React.useState(0);
	const [ color, setColor ] = React.useState(car_color[0]);
	const { t } = useTranslation();
	return (
		<StyledCard >
		<div onClick={onClick}>
		<CardItemElement>
		<Text type={'Subtitle-1'}>{t('product.make')}{': '} </Text>
			<Text type={'Subtitle-2'}>{car_make}</Text> </CardItemElement>
			<CardItemElement><Text type={'Subtitle-1'}>{t('product.model')}{': '} <Text/></Text> <Text type={'Subtitle-2'}>{car_model}</Text></CardItemElement>
			<CardItemElement> <Text type={'Subtitle-1'}>{'VIN'}{': '} </Text> <Text type={'Subtitle-2'}>{car_vin}</Text></CardItemElement>
			<CardItemElement> <Text type={'Subtitle-1'}>{t('product.year')}{': '} </Text><Text type={'Subtitle-2'}>{car_year}</Text></CardItemElement>
			<CardItemElement> <Text type={'Subtitle-1'}>{t('product.price')}{': '}</Text><Text type={'Subtitle-2'}>{car_price}</Text></CardItemElement>
			</div>
			<QtyCounter disabled={qty<=0} onClick={() => setQty((prevValues) => prevValues - 1)}>-</QtyCounter>
			{qty}
			<QtyCounter onClick={() => setQty((prevValues) => prevValues + 1)}>+</QtyCounter>

			<div style={{ marginTop: 10 }}>
				<select
				value={color}
					onChange={(e) =>
						setColor(e.target.value)
					}
				>
				{	car_color?.map((color, item)=>{
						return (<option key={item} value={color} name={color}>
							{color}
						</option>)
					})}
				</select>
			</div>
			<AddToCart disabled={qty<=0} onClick={()=>{addToCart({...item, qty, car_color:color})
			setQty(0)
			}}>{t('addToCart')}</AddToCart>
			
		</StyledCard>
	);
};

export default Card;
