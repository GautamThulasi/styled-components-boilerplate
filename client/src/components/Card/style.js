import { css, styled } from '../../style';

export const StyledCard = styled.div`
	margin: 10px;
	padding: 10px;
	font-stretch: normal;
	font-style: normal;
	color: ${({ theme }) => theme.colors.primary.base};
	min-height: 300px;
	border-radius: 4px;
	flex: 0 0 25%;
	box-shadow: 1px 1px 1px 1px grey;
	&:hover {
		cursor: pointer;
		opacity: .6;
	}
`;

export const QtyCounter = styled.button`
	color: #ffffff;
	border: 0;
	cursor: pointer;
	border-radius: 4px;
	width: 25px;
	height: 25px;
	margin: 10px;
	background-color: ${({ theme }) => theme.colors.button.info};
	font-size: 20px;
	&:hover {
		opacity: .8;
	}
`;

export const CardItemElement = styled.div`
	display: flex;
	flex-direction: row;
	margin: 0 10px 0 10px;
	align-items: center;
`;

export const AddToCart = styled.button`
	color: #ffffff;
	background-color: ${({ theme, disabled }) =>
		!disabled ? theme.colors.button.primary : theme.colors.button.secondary};
	border: 0;
	cursor: ${({ disabled }) => (!disabled ? 'pointer' : 'default')};
	border-radius: 4px;
	margin: 10px 10px 10px 0;
	width: 100%;
	height: 40px;
	&:hover {
		opacity: ${({ disabled }) => (!disabled ? 0.8 : 1)};
	}
`;
