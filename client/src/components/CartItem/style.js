import { css, styled } from '../../style';

export const StyledCartItem = styled.div`
	margin: 10px;
	padding: 10px;
	font-stretch: normal;
	font-style: normal;
	/* background: ${({ theme }) => theme.colors.neutrals['1']}; */
	color: ${({ theme }) => theme.colors.primary.base};
	min-height: 100px;
	max-width: 200px;
	border-radius: 4px;
	flex: 1;
	box-shadow: 1px 1px 1px 1px grey;
`;

export const DeleteCartItem = styled.button`
	color: #ffffff;
	background-color: ${({ theme }) => theme.colors.button.primary};
	border: 0;
	cursor: pointer;
	border-radius: 4px;
	margin: 10px 10px 10px 0;
	width: 100%;
	height: 40px;
	&:hover {
		opacity: .8;
	}
`;
