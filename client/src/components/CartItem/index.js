import React from 'react';
import { StyledCartItem, DeleteCartItem } from './style';
import Text from '../Text';

const CartItem = ({ item, removeFromCart }) => {
	const { car_color, car_img, car_make, car_model, car_price, car_vin, car_year, id, qty } = item;
	return (
		<StyledCartItem>
			<Text type={'Heading-5'}>{car_make}</Text>
			<Text type={'Heading-6'}>{car_price}</Text>
			<Text type={'Heading-6'}>{qty}</Text>
			<Text type={'Heading-6'}>{car_color}</Text>
			<DeleteCartItem onClick={() => removeFromCart({ ...item })}>{'REMOVE'}</DeleteCartItem>
		</StyledCartItem>
	);
};

export default CartItem;
