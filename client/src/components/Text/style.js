import { css, styled } from '../../style';

export const styles = css`
	margin: 0;
	padding: 0;
	font-stretch: normal;
	font-style: normal;
	${({ theme, type }) => {
		return theme.text.types[type];
	}} display: ${({ inline }) => (inline ? 'inline-block' : 'block')};
`;

export const StyledH1 = styled.h1`${styles};`;
export const StyledH2 = styled.h2`${styles};`;
export const StyledH3 = styled.h3`${styles};`;
export const StyledH4 = styled.h4`${styles};`;
export const StyledH5 = styled.h5`${styles};`;
export const StyledH6 = styled.h6`${styles};`;
export const StyledSubtitle1 = styled.div`${styles};`;
export const StyledSubtitle2 = styled.div`${styles};`;
export const StyledBody1 = styled.div`${styles};`;
export const StyledBody2 = styled.div`${styles};`;

export const ParagraphStyled = styled.div`${styles};`;
