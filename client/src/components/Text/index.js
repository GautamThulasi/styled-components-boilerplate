import React from 'react';
import {
	StyledH1,
	StyledH2,
	StyledH3,
	StyledH4,
	StyledH5,
	StyledH6,
	StyledSubtitle1,
	StyledSubtitle2,
	StyledBody1,
	StyledBody2,
	ParagraphStyled
} from './style';
const TextField = ({ type, children }) => {
	const TextComponent =
		{
			'Heading-1': StyledH1,
			'Heading-2': StyledH2,
			'Heading-3': StyledH3,
			'Heading-4': StyledH4,
			'Heading-5': StyledH5,
			'Heading-6': StyledH6,
			'Subtitle-1': StyledSubtitle1,
			'Subtitle-2': StyledSubtitle2,
			'Body-Default': StyledBody1,
			'Body-Small': StyledBody2,
			paragraph: ParagraphStyled
		}[type] || ParagraphStyled;
	return <TextComponent type={type}>{children}</TextComponent>;
};

export default TextField;
