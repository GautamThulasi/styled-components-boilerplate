import { put } from './axios';
const productsListing = async (page) => {
	return put('api/v1/productsListing', { page, pageSize: 10 });
};

export { productsListing };
