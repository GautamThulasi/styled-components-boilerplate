import { put } from './axios';
const fetchArticle = async (page) => {
	return put('api/v1/news', { page });
};

export { fetchArticle };
