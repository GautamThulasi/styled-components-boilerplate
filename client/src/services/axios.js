import axios from 'axios';

const getToken = () => null;

const defaultHeaderProps = {
	'Cache-Control': 'no-cache',
	Accept: 'application/json',
	'Content-Type': 'application/json'
};

export async function get(url, payload) {
	let token = getToken();
	const response = await axios(url, {
		method: 'GET',
		headers: defaultHeaderProps
	});
	return response;
}

export async function post(url, payload) {
	let token = getToken();
	const response = await axios(url, {
		method: 'POST',
		headers: defaultHeaderProps,
		data: payload
	});
	return response;
}

export async function put(url, payload) {
	let token = getToken();
	const response = await axios(url, {
		method: 'PUT',
		headers: defaultHeaderProps,
		data: payload
	});

	return response;
}
